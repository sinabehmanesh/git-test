package main

import "fmt"

//Server this is the struct
type Server struct {
	Name string
	IP   string
	OS   string
	UP   bool
}

//Ipvalidation function
func (s Server) Ipvalidation(m Server) bool {
	if len(s.IP) > 7 || len(s.IP) < 15 {
		return true
	} else {
		return false
	}
}

//Isup function (is server up?)
func (s Server) Isup(m Server) bool {
	if m.UP == true {
		//fmt.Println("Server ", m.Name, "is UP!")
		return true
	}
	//  fmt.Println("SERVER ", m.Name, "IS DOWN!!")
	return false
}
func main() {
	s1 := Server{Name: "master", IP: "0.0.0.0", OS: "ubuntu", UP: true}
	fmt.Println(s1.Name)
	s1.Isup(s1)
	fmt.Println("is server IP valid?")
	fmt.Println(s1.Ipvalidation(s1), "ip ", s1.IP, " is valid")
	fmt.Println("commited to the git!")
}
